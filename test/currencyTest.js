const server = require('./../index');
const chai = require('chai');
const chaihttp = require('chai-http');
const should = chai.should();
// get Currency Model
const Currency = require('./../models/currency');
const Rate = require('../models/rate');


afterEach(done=>{
    Currency.deleteMany({})
    .then((data)=>{
        console.log(data)
        done();
    })
    .catch((err)=>{
        console.log(err)
        done();
    })
})

chai.use(chaihttp);
chai.should();


// 1. get the Root path
describe('/ GET root path', () => {
    it("should get the root path", (done)=> {
        chai.request(server)
        .get('/')
        .end((err, res)=> {
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal('welcome to api');
            done();
        })
    })
})

// 2. Get the Currency show list
describe('/GET show Currency list', ()=> {
    it("should get the show currency list", (done)=>{
        chai.request(server)
        .get('/api/currency/')
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal('Currency List:');
            done();
        })
    })
})