const server = require('../index');
const chai = require('chai');
const chaihttp = require('chai-http');
const should = chai.should();
// get Rate Model
const Rate = require('../models/rate');

afterEach(done=>{
    Rate.deleteMany({})
    .then((data)=>{
        console.log(data)
        done();
    })
    .catch((err)=>{
        console.log(err)
        done();
    })
})

chai.use(chaihttp);
chai.should();


// 1. get the rate show list
describe('/GET Rate Routes', () => {
    it("it should get Rate List", (done)=> {
        chai.request(server)
        .get('/api/rate/')
        .end((err, res)=> {
            res.should.have.status(200);
            res.should.be.an("object");
            done();
        })
    })
})


