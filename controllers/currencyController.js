const Currency = require('../models/currency');
const { success, errorMessage } = require('../helper/response');
const { validationCurrency } = require('../helper/validation');

// 1. Create new Currency
async function createCurrency(req, res){
    // New and Create Function
    const currency = new Currency({from: req.body.from, to: req.body.to});
    
    // save new currency in currency schema
    const currency_result = await currency.save();
    if(!currency_result) return res.status(422).json(errorMessage("failed to create new Currency"))

    res.status(200).json(success(currency_result, "Success create new Currency !"))
}

// 2. Show all Currency
async function showAll(req, res){
    // show all Currency
    const currencyList = await Currency.find({});
    res.status(200).json(success(currencyList, "Currency List:"))
}

// 3. find by Currency
async function show_by_currency(req, res){
    const showCurrency = await Currency.find({ from: req.body.from, to: req.body.to})

    if(!showCurrency) return res.status(422).json(errorMessage("there is no such data on database"));
    res.status(200).json(success(showCurrency, `this is the exchange rate from ${req.body.from} to ${req.body.to}`));
}


module.exports = { createCurrency, showAll, show_by_currency}
