const Currency = require('../models/currency');
const Rate = require('../models/rate');
const { success, errorMessage } = require('../helper/response');
const { validationRate } = require('../helper/validationRate');
const _ = require('lodash');

// 1. create new Rate
async function newRate(req, res) {

    let currency = await Currency.findOne({from: req.body.from, to: req.body.to})
    if(!currency) return res.status(422).status(errorMessage("cannot found currency"));
    console.log(currency)

    // New & Create function
    let newRate = new Rate({currency: currency, 
        date: req.body.date, rate: req.body.rate }).populate(['currency', 'from', 'to']);
    // saving new rate
    const result = await newRate.save();
    
    if(!result) return res,status(422).json(errorMessage("failed to create new Currency"));
    res.status(200).json(success(result, "successfully create new Currency"))
}


// 2. show all currency
async function showAll(req, res) {
    // show all rate
    const rateList = await Rate.find({});
    res.status(200).json(success(rateList, "here is the currency list"));
}


// 3. find by Date
async function show_by_date(req, res){
    /** GROUP BY DATE **/
    const date = new Date(req.body.date)
    const lastWeek = new Date(date.setDate(date.getDate() - 6));
    console.log(lastWeek)
    let showDate = await Rate.find({date: 
        {
            $gte: lastWeek,
            $lte: req.body.date
        }
    }).populate(['currency', 'from', 'to']);

    // get the rate
    let rate  = _.chain(showDate)
    .groupBy('currency')
    .map((value, key)=> ({currency: key, rate: value}))
    .value()

    // get the average
    let average = _.chain(showDate)
    .groupBy('currency')
    .map((value, key)=> ({currency: key, average: _.meanBy(value, 'rate')}))
    .value()
    console.log(average)

    // get the max
    let max = _.chain(showDate)
    .groupBy('currency')
    .map((value, key)=> ({currency: key, max: _.maxBy(value, 'rate')}))
    .value()
    console.log(max)

    
    // get the min
    let min = _.chain(showDate)
    .groupBy('currency')
    .map((value, key)=> ({currency: key, min: _.minBy(value, 'rate')}))
    .value()
    console.log(min)

    // get the variance
    let variance = await (max-min)
    console.log(variance, "--variance")

    // get the result



    let result = {
        // from: ,
        // to: ,
        // rate: , ￼
        // rate: rate,
        average: average
    }

    // if failed to show date
    if(!result) return res.status(422).json(errorMessage(`cannot find data with ${req.body.date}`));
    // if success get data
    res.status(200).json(success(result, `Currency data based on ${req.body.date}`))

    // group by date
}


// 4. Delete the Currency From to
function removeRate(rate, elem){
    var index = rate.indexOf(elem);
    if(index > -1){
        rate.splice(index, 1);
    }
}
async function delete_rate(req, res){
    let rate = await Rate.findById(req.params.id);
    console.log(rate)    
            /** DELETE TODO FROM Currency SCHEMA */
    // get the currency data from token
    currency = await Currency.findOne({rate: req.params.id});
    console.log(currency)
    // delete todo from user index
    removeRate(currency.rate, rate._id);

    // delete the todo from Todo Index
    const deleteRate = await Rate.findByIdAndDelete(req.params.id)

    if(!deleteRate) return res.status(422).json(errorMessage(`cannot delete data with Rate ${req.body.rate}, and Date ${req.body.date}`));
    res.status(200).json(success(deleteRate, `success delete data with Rate ${req.body.rate}, and Date ${req.body.date}`))
}


// 5. Update the Currency from params Id
async function update_rate(req, res){
    const populateQuery = [{path: 'currency', select: 'from'}, {path: 'currency', select:'to'}];
    // find Rate at Currency shema
    let currency = await Currency.findOne({rate: req.params.id});
    if(!currency) return res.status(422).json(errorMessage("cannot find rate ID"));

    const updateRate = await Rate.findByIdAndUpdate(req.params.id, 
        {
            $set: {date: req.body.date, rate: req.body.rate}
        }, {new: true}.populate(populateQuery) )
    
    if(!updateRate) return res.status(422).json(errorMessage("fail to update"));
    res.status(200).json(success(updateRate, "success update Rate"))
}

module.exports = { newRate, showAll, show_by_date,
    delete_rate, update_rate };
