const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const currencySchema = new Schema({
    from: {
        type: String,
        required: true,
        uppercase: true
    },
    to: {
        type: String,
        required: true,
        uppercase: true
    }
})


const Currency = mongoose.model("Currency", currencySchema);

module.exports = Currency;


