const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rateSchema = new Schema({
    currency: {type: Schema.Types.ObjectId, ref: 'Currency'},
    date: {
        type: Date,
        required: true
    },
    rate: {
        type: Number
    }
})


const Rate = mongoose.model("Rate", rateSchema);

module.exports = Rate;
