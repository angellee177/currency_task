const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// for encrypt the password
const bcrypt = require('bcrypt');
const saltRounds = 10;
// get the jwtPrivateKey
const jwt = require('jsonwebtoken');
// get the configuration
const config = require('config');

const userSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            lowercase: true,
            required: true,
            validate: function(email){
                return /^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)
            }
        },
        password: {
            type: String,
            required: true
        }
    }
)

// encrypt the password before user save the register data
userSchema.pre('save', function(next){
    let user = this
    user.password = bcrypt.hashSync(user.password, saltRounds);
    next();
})


// generate jwt token
userSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id: this._id, name: this.name, email: this.email}, config.get('jwtPrivateKey'))
    return token
}


const User = mongoose.model("User", userSchema);

module.exports = User;

