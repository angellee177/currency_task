const router = require('express').Router();
const rate_controller = require('../controllers/rateCotroller');

router.post('/new', rate_controller.newRate);
router.get('/', rate_controller.showAll);
router.get('/find-by-date', rate_controller.show_by_date);
router.delete('/delete/:id', rate_controller.delete_rate);
router.put('/update/:id', rate_controller.update_rate);


module.exports = router;

