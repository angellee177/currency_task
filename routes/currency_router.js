const router = require('express').Router();
const currencyController = require('../controllers/currencyController');

router.post('/new', currencyController.createCurrency);
router.get('/', currencyController.showAll);
router.get('/find', currencyController.show_by_currency);

module.exports = router;

