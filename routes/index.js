const router = require('express').Router();
const userRouter  = require('./user_router');
const rateRouter = require('./rate_router');
const currencyRouter = require('./currency_router');

router.use('/user', userRouter);
router.use('/rate', rateRouter);
router.use('/currency', currencyRouter);

module.exports = router;