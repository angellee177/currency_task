const Joi = require('@hapi/joi');

// for validate currency Function
function validationRate(rateSchema){
    const schema = {
        date: Joi.date().required(),
        rate: Joi.number().required()
    }
    return Joi.validate(rateSchema, schema)
}


module.exports = { validationRate };