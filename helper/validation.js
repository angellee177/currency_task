const Joi = require('@hapi/joi');

// for validate currency Function
function validationCurrency(currencySchema){
    const schema = {
        from: Joi.string().required(),
        to: Joi.string().required()
    }
    return Joi.validate(currencySchema, schema)
}


module.exports = { validationCurrency };