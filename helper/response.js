const success = (result, message ) => {
    return {
        success: true,
        message: message,
        result: result
    }
}


const errorMessage = (err) => {
    return {
        success: false,
        message: err
    }
}

module.exports = { success, errorMessage };

